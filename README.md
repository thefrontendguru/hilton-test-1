# Hilton Test 1

## Getting Started
Open the src folder and open the `index.html` in any browser.

## Assumptions:
### Screen Size
I assumed that this was for phones only, so any screen bigger 
768px gets a special "empty state" messaage.

### RTL
Im also assuming the user base is world wide, so I factored in RTL
into the codebase by using flex. By adding "directions: rtl" to the 
body tag, the page will adjust seemlessly.

### Browser compatibility
Again assuming a world-wide user base, I have factored in compatibility
with the last 2 versions of Firefox, Chrome, IE, and Safari. We achieve this
by pointing to the "index_autoprefixed.css" in the "index.html", keeping 
the "index_raw.css" untouched incase we was to re-use the css classes for 
creating react components in the future.

### Extraneous
I added more navigational item to illustrate page scroll-abilty. 

Also, I did my best to keep the codebase light-weight and use unicode
characters for icons instead of bringing in a big font lib for 1-2 icons.
